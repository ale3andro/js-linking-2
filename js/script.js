// Code from this video tutorial
// https://www.youtube.com/watch?v=LE3KKExJoP0
// Code 
// https://gist.github.com/productivity-for-programmers/6ef09ef5ccd5ab62a2d259d4c7260376

/** FROM https://www.kevinleary.net/javascript-get-url-parameters/
 * JavaScript Get URL Parameter
 * 
 * @param String prop The specific URL parameter you want to retreive the value for
 * @return String|Object If prop is provided a string value is returned, otherwise an object of all properties is returned
 */
function getUrlParams( prop ) {
    var params = {};
    var search = decodeURIComponent( window.location.href.slice( window.location.href.indexOf( '?' ) + 1 ) );
    var definitions = search.split( '&' );

    definitions.forEach( function( val, key ) {
        var parts = val.split( '=', 2 );
        params[ parts[ 0 ] ] = parts[ 1 ];
    } );

    return ( prop && prop in params ) ? params[ prop ] : params;
}

// https://stackoverflow.com/a/2117523
function uuidv4() {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
    var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
    return v.toString(16);
  });
}

function setupJsPlumb() {
  instance = jsPlumb.getInstance({});
  instance.setContainer("diagram");
  instance.bind("ready", function () {
    instance.registerConnectionTypes({
      "red-connection": {
        paintStyle: {stroke: "red", strokeWidth: 5},
        hoverPaintStyle: {stroke: "red", strokeWidth: 10},
        connector: "Bezier"
      },
      "green-connection": {
          paintStyle: {stroke: "green", strokeWidth: 5},
          hoverPaintStyle: {stroke: "green", strokeWidth: 10},
          connector: "Bezier"
        }
    });
    for (i=0; i<draggables.length; i++) {
      var elementName = 'controlA' + i;
      instance.draggable(elementName, {"containment": true});
      instance.addEndpoint(elementName, {
        endpoint: "Dot",  // rectangle, blank, image
        anchor: ["Bottom"],
        isSource: true,
        connectionType: "red-connection"
      });
    }
    for (i=0; i<droppables.length; i++) {
      var elementName = 'controlB' + i;
      instance.draggable(elementName, {"containment": true});
      instance.addEndpoint(elementName, {
        endpoint: "Dot",
        anchor: ["Top"],
        isTarget: true,
        connectionType: "red-connection"
      });
    }
    
    // https://stackoverflow.com/a/4502207
    instance.bind("contextmenu", function (component, event) {
      if (component.hasClass("jtk-connector")) {
        event.preventDefault();
        window.selectedConnection = component;
        $("<div class='custom-menu'><button class='delete-connection'>Διαγραφή σύνδεσης</button></div>")
          .appendTo("body")
          .css({top: event.pageY + "px", left: event.pageX + "px"});
      }
    });
    $("body").on("click", ".delete-connection", function (event) {
      instance.deleteConnection(window.selectedConnection);
    });

    $(document).bind("click", function (event) {
      $("div.custom-menu").remove();
    });

    $("#delete").bind("click", function (event) {
      instance.deleteEveryConnection();
    });

    $("#check").bind("click", function (event) {
      var answer = [];
      for (i=0; i<instance.getAllConnections().length; i++) {
        targetNum = parseInt(instance.getAllConnections()[i].targetId[instance.getAllConnections()[i].targetId.length -1]);
        sourceNum = parseInt(instance.getAllConnections()[i].sourceId[instance.getAllConnections()[i].sourceId.length -1]);
        //console.log("Source:" + sourceNum + " Target: " + targetNum);
        answer[targetNum] = sourceNum;
      }
      console.log("User thinks: " + answer.join(""));
      console.log("Correct: " + metadata[2]);

      function killSadGif() {
        $('#sadgif').remove();
      }

      if (answer.join("")==metadata[2]) {
        $('#diagram').html("<img src='img/happy_winner_big.gif'>");
        $('#sadgif').remove();
      }
      else {
        if (!$('#sadgif').length) {
          $('#toolbox').append("<span id='sadgif'><img src='img/no-emoji.gif'><span>");
          const myTimeout = setTimeout(killSadGif, 5000);
        }
      }
    });

    $("body").on("contextmenu", "#diagram .control", function (event) {
      event.preventDefault();
      window.selectedControl = $(this).attr("id");
      $("<div class='custom-menu'><button class='delete-control'>Delete control</button></div>")
        .appendTo("body")
        .css({top: event.pageY + "px", left: event.pageX + "px"});
    });

    $("body").on("click", ".delete-control", function (event) {
      instance.remove(window.selectedControl);
    });

    $("#toolbox .control").draggable({
      helper: "clone",
      containment: "body",
      appendTo: "#diagram"
    });

    $("#diagram").droppable({
      drop: function(event, ui) {
        var id = uuidv4();
        var clone = $(ui.helper).clone(true);
        clone.attr("id", id);
        clone.appendTo(this);
        instance.draggable(id, {containment: true});

        instance.addEndpoint(id, {
          endpoint: "Dot",
          anchor: ["RightMiddle"],
          isSource: true,
          connectionType: "red-connection"
        });

        instance.addEndpoint(id, {
          endpoint: "Dot",
          anchor: ["LeftMiddle"],
          isTarget: true,
          connectionType: "red-connection"
        });
      }
    })

  });
}

var metadata = [];
var draggables = [];
var droppables = [];

$( document ).ready(function() {
  $.ajax(
    {   url: "matching/" + getUrlParams('id') + ".json",
        success: function(data) {
            var draggables_count = 0;
            var droppables_count = 0;
            metadata[0] = data['metadata']['title'];
            metadata[1] = data['metadata']['class'];
            metadata[2] = data['metadata']['answer'];
            $('#message').html(metadata[0]);
            document.title = "Τάξη " + metadata[1] + " | " + metadata[0];
            // Clear
            $('#diagram').html('');
            $.each(data['draggables'], function(index, value) {
                draggables[draggables_count] = value;
                draggables_count++;
                var element = '<div id="controlA' + value.id + '" class="control" style="top: 10px; left: ' + (10+140*(draggables_count-1)) + 'px"><img src="' + value.img_src + '" width="100px" height="100px"></div>';  
                $('#diagram').append(element);
            });
            $.each(data['droppables'], function(index, value) {
                droppables[droppables_count] = value;
                droppables_count++;  
                var element = '<div id="controlB' + value.id + '" class="control controlTarget d-flex align-items-center justify-content-center" style="top: 300px; left: ' + (10+140*(droppables_count-1)) + 'px">' + value.text + '</div>';
                $('#diagram').append(element);
            });
            setupJsPlumb();
        },
        fail: function(result) {
            if (debugging) {
                console.log( "Αδυναμία φόρτωσης του json αρχείου από τον server" );
                console.log(jqxhr);
            }  
        } 
});
});